@extends('layouts.app')

@section('title' , 'Editar Funcionário')

@section('content')

    @include('employees.partials.edit.editForm')

@endsection