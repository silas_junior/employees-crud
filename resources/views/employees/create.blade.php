@extends('layouts.app')

@section('title' , 'Cadastrar Funcionário')

@section('content')

    @include('employees.partials.create.formCreate')

@endsection