<div class="container">

<div class="title">
    <h4>Novo Funcionário</h4>
</div>

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form">            
    <form action="" method="post">
    @csrf
        <div class="form-group">

            <div class="row">
                <div class="col-md-5">
                    <label for="">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Digite seu nome" value="{{ old('name') }}">
                </div>

                <div class="col-md-6">
                    <label for="">Sobrenome</label>
                    <input type="text" class="form-control" name="lastname" placeholder="Digite seu sobrenome" value="{{ old('lastname') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label for="">Idade</label>
                    <input type="text" class="form-control" name="age" placeholder="Idade" value="{{ old('age') }}">
                </div>

                <div class="col-md-6">
                    <label for="">Gênero</label>
                    <select name="gender" id="" class="form-control">
                        <option value="">Selecione uma opção</option>
                        @foreach($genders as $gender)
                            <option value="{{ $gender }}">{{ $gender }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Cadastrar</button>
    </form>
</div>

</div>