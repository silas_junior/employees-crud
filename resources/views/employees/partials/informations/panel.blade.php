<div class="container">
    <a href="{{ route('employee.index') }}">Voltar para Página Inicial</a>
    <h4>Informações Usuários</h4>
    
    <div class="list-information">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Funcionários Sexo Masculino</th>
                    <th>Funcionários Sexo Feminino</th>
                    <th>Funcionário(a) mais velho</th>
                    <th>Funcionário(a) mais novo</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $male }}</td>
                    <td>{{ $feminine }}</td>
                    <td>{{ $oldEmployee->name . ' ' . $oldEmployee->lastname }}</td>
                    <td>{{ $youngEmployee->name . ' ' . $youngEmployee->lastname }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>