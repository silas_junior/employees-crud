<div class="list-employees">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Sobrenome</th>
                <th>Idade</th>
                <th>Gênero</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employees as $employee)        
            <tr>
                <td>{{ $employee->id }}</td>
                <td>{{ $employee->name }}</td>
                <td>{{ $employee->lastname }}</td>
                <td>{{ $employee->age }}</td>
                <td>{{ $employee->gender }}</td>
                <td>
                    <a href="{{ route('employee.edit' , [ $employee->id ]) }}" class="btn btn-primary">Editar</a>
                    <form action="{{ route('employee.destroy' , $employee->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="hidden" name="employee">
                        <input type="submit" class="btn btn-danger" id="excluir" value="Excluir" onclick="return confirm('Deseja mesmo excluir {{ $employee->name . ' ' . $employee->lastname}} ?')">
                    </form>
                </td>
            </tr>
            @endforeach            
        </tbody>
    </table>
</div>