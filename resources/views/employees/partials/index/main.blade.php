<div class="main">
    <div class="new-user">
        <a href="{{ route('employee.create') }}" class="btn btn-success">Cadastrar Funcionário</a>
    </div>

    <div class="more-information">
        <a href="{{ route('employee.informations') }}" class="btn btn-primary">Mais Informações</a>
    </div>
</div>