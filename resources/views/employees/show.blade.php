@extends('layouts.app')

@section('title' , 'Informações Funcionários')

@section('content')

    @include('employees.partials.informations.panel')

@endsection