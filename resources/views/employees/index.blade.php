@extends('layouts.app')

@section('title', 'Página Inicial')

@section('content')

    @include('employees.partials.index.header')
    @include('employees.partials.index.main')
    @include('employees.partials.index.dataTable')

@endsection