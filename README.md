<p><h1>CRUD - Laravel</h1</p>



## Primeiros Passos

Para iniciar o projeto será necessário a instalação de algumas ferramentas que serão listadas logo abaixo:

- [Xampp, para ativar o apache e o banco de dados](https://www.apachefriends.org/pt_br/download.html).
- [MySql Workbench, para criar o banco de dados que será usado para armazenar as informações](https://dev.mysql.com/downloads/workbench/).
- [Composer, para instalar dependências](https://getcomposer.org/download/).

-Logo após instalar as ferramentas, abra o Xampp e deixe ativadas as opções "Apache" e "MySQL".


## Clonando Repositório

Depois de ter instalado todas as ferramentas necessárias, a primeira coisa a se fazer é executar o "git clone" deste repositório no diretório de sua preferência.

## Criando Servidor Local

Depois disto, abra a pasta em que se encontra o projeto na ide ou editor de texto de sua preferência e execute no terminal "php artisan serve"(Deixe esse terminal rodando este comando e abra um novo terminal e navegue até a mesma pasta.), esse comando irá criar um localhost na sua máquina, copie-o e cole na barra de busca do seu browser. 

Será exibido um erro, esse erro irá ocorrer pelo fato de não ter configurado seu banco de dados.

## Configurações Banco de Dados

Configure as credenciais do seu banco de dados no arquivo ".env.example" e logo após renomeie-o para ".env".

Logo após, execute no terminal "php artisan migrate" para rodar as migrations e criar as tabelas. Por padrão, o laravel traz algumas migrations prontas, mas não se preocupe, você irá utilizar somente a migration referente a tabela "employees" e "genders".

## Adicionando registros com Seed

Para aplicar alguns registros na tabela "employees", execute no terminal o comando "php artisan db:seed --class==CreateEmployeeSeed".

Para aplicar alguns registros na tabela "genders", execute no terminal o comando "php artisan db:seed --class==GenderEmployeeSeed".

Precisaremos também de uma outra tabela necessária para seleção de Gêneros. execute no terminal o comando "composer dump-autoload" e logo após execute "php artisan db:seed".

## Rodando Aplicação
Depois de ter feito todos esses passos, a aplicação está pronta para funcionar, basta somente reiniciar a página do browser em que foi colada o servidor criado por "php artisan serve".
