<?php

use Illuminate\Support\Facades\Route;


Route::get('/' , function (){
    return redirect()->route('employee.index');
});

Route::get('/funcionarios', 'EmployeeController@index')->name('employee.index');
Route::get('/funcionario/cadastro' , 'EmployeeController@create')->name('employee.create');
Route::post('/funcionario/cadastro' , 'EmployeeController@store')->name('employee.store');
Route::get('/funcionarios/informacoes' , 'EmployeeController@show')->name('employee.informations');
Route::get('/funcionario/{employee}/edit' , 'EmployeeController@edit')->name('employee.edit');
Route::put('/funcionario/{employee}' , 'EmployeeController@update')->name('employee.update');
Route::delete('/funcionario/{employee}' , 'EmployeeController@destroy')->name('employee.destroy');
