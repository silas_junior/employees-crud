<?php

use Illuminate\Database\Seeder;
use App\Employee;

class CreateEmployeeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->employeeSeed();
    }

    public function employeeSeed() {
        Employee::create([
            'id' => '1',
            'name' => 'José',
            'lastname' => 'SaphirTI',
            'age' => '26',
            'gender' => 'Masculino'
        ]);

        Employee::create([
            'id' => '2',
            'name' => 'Lucas',
            'lastname' => 'SaphirRH',
            'age' => '27',
            'gender' => 'Masculino'
        ]);

        Employee::create([
            'id' => '3',
            'name' => 'Silas',
            'lastname' => 'Pereira',
            'age' => '20',
            'gender' => 'Masculino'
        ]);
    }
}
