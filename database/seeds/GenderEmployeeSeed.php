<?php

use Illuminate\Database\Seeder;
use App\Gender;

class GenderEmployeeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->genderSeed();
    }

    public function genderSeed() {

        Gender::create(['id' => '1', 'name' => 'Masculino']);
        Gender::create(['id' => '2', 'name' => 'Feminino']);

    }
}
