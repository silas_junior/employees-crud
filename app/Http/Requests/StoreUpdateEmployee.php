<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha|min:3|max:40',
            'lastname' => 'required|alpha|min:3|max:40',
            'age' => 'required|min:2|max:3',
            'gender' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo "Nome" é obrigatório',
            'name.alpha' => 'Campo "Nome" não aceita números ou espaços',
            'name.min' => 'Campo "Nome" requer ao menos 3 caracteres',
            'name.max' => 'Campo "Nome" contém mais de 40 caracteres',

            'lastname.required' => 'Campo "Sobrenome" é obrigatório',
            'lastname.alpha' => 'Campo "Sobrenome" não aceita números ou espaços',
            'lastname.min' => 'Campo "Sobrenome" requer ao menos 3 caracteres',
            'lastname.max' => 'Campo "Sobrenome" contém mais de 40 caracteres',

            'age.required' => 'Campo "Idade" é obrigatório',
            'age.numeric' => 'Campo "Idade" aceita somente letras',
            'age.min' => 'Campo "Idade" requer ao menos 2 caracteres',
            'age.max' => 'Campo "Idade" contém mais de 3 caracteres',

            'gender.required' => 'Campo "Gênero" é obrigatório'
        ];
    }
}
