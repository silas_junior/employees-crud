<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Gender;
use App\Http\Requests\StoreUpdateEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();

        return view('employees.index', [
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genders = Gender::pluck('name', 'id');

        return view('employees.create' , [
            'genders' => $genders
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreUpdaateEmployee  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateEmployee $request)
    {
        $employee = ( new Employee() )->fill($request->toArray());
        $employee->save();

        return redirect()->route('employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $queryMedia = DB::select('select avg(age) from employees where age >= 14');
        $media = $queryMedia[0];

        $queryMale = DB::select('select id from employees where gender = "Masculino"');
        $male = count($queryMale);

        $queryFeminine = DB::select('select id from employees where gender = "Feminino"');
        $feminine = count($queryFeminine);

        $queryOld = DB::select('select name, lastname from employees order by age');
        krsort($queryOld);
        $oldEmployee = current($queryOld);

        $queryYoung = DB::select('select name, lastname from employees order by age');
        krsort($queryYoung);
        $youngEmployee = $queryYoung[0];
        

        return view('employees.show')
        ->with('media' , $media)
        ->with('male' , $male)
        ->with('feminine' , $feminine)
        ->with('oldEmployee' , $oldEmployee)
        ->with('youngEmployee' , $youngEmployee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $genders = Gender::pluck('name' , 'id');

        return view('employees.edit' , [
            'employee' => $employee,
            'genders' => $genders
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\StoreUpdateEmployee  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateEmployee $request, Employee $employee)
    {
        $employee->fill($request->toArray());
        $employee->update();

        return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employee.index');
    }
}
